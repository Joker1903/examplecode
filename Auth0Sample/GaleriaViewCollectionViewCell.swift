//
//  GaleriaViewCollectionViewCell.swift
//  GuiApp
//
//  Created by Fernando Garay on 27/08/2018.
//   
//

import UIKit

class GaleriaViewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
