//
//  Imagenes.swift
//  GuiApp
//
//  Created by Fernando Garay on 18/05/2018.
//   
//

import Foundation
import SwiftyJSON

class Imagenes {
    
    let imagenes : [Imagen]
    
    init (json: JSON){
        
        var auxImagenes = [Imagen]()
        
        for (_,subjson):(String, JSON) in json[]{
            var imagen : Imagen
            imagen = Imagen(json: subjson)
            auxImagenes.append(imagen)
        }
        imagenes = auxImagenes
    }
}
