//
//  Categoria.swift
//  GuiApp
//
//  Created by Fernando Garay on 16/05/2018.
//   
//

import Foundation
import SwiftyJSON


class Categoria {
    
    var id : String
    var titulo: String
    
    init(){
        id = ""
        titulo = ""
    }
}
