//
//  Eventos.swift
//  GuiApp
//
//  Created by Fernando  on 2/27/18.
//   
//

import Foundation
import SwiftyJSON

class Eventos {
    
    let eventos : [Evento]
    
    init (json: JSON){
        
        var auxEnventos = [Evento]()
        
        for (_,subjson):(String, JSON) in json[]{
            var evento: Evento
            evento = Evento(json: subjson)
            auxEnventos.append(evento)
        }
        eventos = auxEnventos
    }
}
