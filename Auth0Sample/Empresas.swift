//
//  Empresas.swift
//  GuiApp
//
//  Created by Fernando Garay on 20/04/2018.
//   
//

import Foundation
import SwiftyJSON

class Empresas {
    
    let empresas : [Empresa]
    
    init (json: JSON){
        
        var auxEmpresas = [Empresa]()
        
        for (_,subjson):(String, JSON) in json[]{
            var empresa : Empresa
            empresa = Empresa(json: subjson)
            auxEmpresas.append(empresa)
        }
        empresas = auxEmpresas
    }
}
