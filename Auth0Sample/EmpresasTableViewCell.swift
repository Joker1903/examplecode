//
//  EmpresasTableViewCell.swift
//  GuiApp
//
//  Created by Fernando Garay on 19/04/2018.
//   
//

import UIKit

class EmpresasTableViewCell: UITableViewCell {
    @IBOutlet weak var razonSocial: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
