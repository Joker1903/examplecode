//
//  MisPublicacionesTableViewCell.swift
//  GuiApp
//
//  Created by Fernando Garay on 6/2/18.
//   
//

import UIKit

class MisPublicacionesTableViewCell: UITableViewCell {
   
    @IBOutlet weak var imgPubli: UIImageView!
    
    @IBOutlet weak var titulo: UILabel!
    
    @IBOutlet weak var descripcion: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
